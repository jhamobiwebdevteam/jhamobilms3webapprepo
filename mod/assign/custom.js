// require(['jquery'],function($){

    $(document).ready(function(){
      
       $(document).on('click','#collapsibleBtn',function(){
            $(".gradingsummary .gradingsummarytable").slideToggle();
       });

        
       $('#page-mod-assign-editsubmission #fitem_id_files_filemanager #id_files_filemanager_label')
       .prepend("<a role='button' class='fa fa-caret-down fa-lg'></a>");
       
       $(document).on('click','#page-mod-assign-editsubmission #fitem_id_files_filemanager #id_files_filemanager_label a',function(){
            $("#page-mod-assign-editsubmission #fitem_id_files_filemanager div[data-fieldtype='filemanager']").slideToggle();
       });
       
       $('#page-mod-assign-editsubmission #fitem_id_onlinetext_editor label[for="id_onlinetext_editor"]')
       .prepend("<a role='button' class='fa fa-caret-down fa-lg'></a>");
       
       $(document).on('click','#page-mod-assign-editsubmission #fitem_id_onlinetext_editor label[for="id_onlinetext_editor"] a',function(){
            $("#page-mod-assign-editsubmission #fitem_id_onlinetext_editor div[data-fieldtype='editor']").slideToggle();
       });


       $("#page-mod-assign-grading .gradingtable .initialbar.firstinitial").toggleClass('d-flex');

       $("#page-mod-assign-grading fieldset#id_general").addClass('collapsed');

       $(document).on('click','#page-mod-assign-grading fieldset#id_general legend.ftoggler a',function(){
         // $("#page-mod-assign-grading fieldset#id_general .fcontainer").slideToggle();
          $("#page-mod-assign-grading .gradingtable .initialbar.firstinitial").toggleClass('d-flex');
     });
    });
// });