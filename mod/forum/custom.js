require(['jquery'],function($){   
    $(document).ready(function(){
        $("time[id^=time-modified").each(function() {
            let postDate = new Date($(this).html()+" 12:00:00").toJSON().slice(0,10).replace(/-/g,'/');
            let curDate = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            if(curDate === postDate){
                $(this).parents("tr").css("background-color","#FFBE51");
            }
        });
    });
});