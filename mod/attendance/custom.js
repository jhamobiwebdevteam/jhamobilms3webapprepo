require(['core/first','jquery','jqueryui','core/ajax'],function(core, $, bootstrap, ajax){

    $(document).ready(function(){
        let button = '<i style="position: absolute;left: -19px;font-size: 2em;top:35px" class="fa fa-caret-down" id="toggleBtn"></i>';
        $("#page-mod-attendance-take #region-main div[role='main']").prepend(button);
       
        $(document).on('click','#toggleBtn',function(){
            $("#page-mod-attendance-take #region-main div[role='main'] .singlebutton").slideToggle();
            $("#page-mod-attendance-take #region-main div[role='main'] .generalbox.takecontrols").slideToggle();
        });
        
    });
});