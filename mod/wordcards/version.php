<?php
/**
 * Version information.
 *
 * @package mod_wordcards
 * @author  Frédéric Massart - FMCorz.net
 */

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'mod_wordcards';
$plugin->version = 2020071700;
$plugin->requires = 2015111603;
$plugin->maturity = MATURITY_STABLE;
$plugin->release = "1.2.2 (Build 2020071700)";