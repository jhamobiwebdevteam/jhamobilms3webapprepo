<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
// $CFG->dbhost    = 'localhost';
$CFG->dbhost    = 'yofundo-db.cxi5nfveoi6s.ap-south-1.rds.amazonaws.com';
$CFG->dbname    = 'moodle39';
// $CFG->dbuser    = 'root';
// $CFG->dbpass    = '';
$CFG->dbuser    = 'admin';
$CFG->dbpass    = 'Yofundo01$';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_unicode_ci',
);
//local data
// $CFG->wwwroot   = 'http://localhost/yofundolmswebapprepo';
// $CFG->dataroot  = 'C:\\moodledata1';

$CFG->wwwroot   = 'https://yofundo.in/lms/clients';
$CFG->sslproxy	= true;
$CFG->dataroot  = '/var/www/moodledata_bk';
$CFG->admin     = 'admin';

//for deleting course contains H5P activity taking more time
$CFG->mod_hvp_backup_libraries = 0;

$CFG->directorypermissions = 0777;

/*
For LCAT version 1.1
$CFG->languageconfidence_clientkey = '2bogl8jqmauu10ns08k45r1arr';
$CFG->languageconfidence_clientsecret = '1rapskudkifa09jvi27fig5g7ddk73o4kong3t101t1cj7jg76fl';
$CFG->languageconfidence_apikey = '7ZH4ZyMz5H1SYjLooMQub94NPPXAoz1O8lAVxvAR';
*/
//for LCAT version 1.2
$CFG->languageconfidence_apikey = '09fc87a133msha9cda7ab7113b63p1082bfjsn62d6adced1b2';

//for course creation error following line is added
// $CFG->defaultblocks_override = 'participants,activity_modules,search_forums,course_list:news_items,calendar_upcoming,recent_activity';
// $CFG->defaultblocks_topics = 'participants,activity_modules,search_forums,course_list:news_items,calendar_upcoming,recent_activity';
// $CFG->defaultblocks_weeks = 'participants,activity_modules,search_forums,course_list:news_items,calendar_upcoming,recent_activity';
// $CFG->defaultblocks = 'participants,activity_modules,search_forums,course_list:news_items,calendar_upcoming,recent_activity';



// @error_reporting(E_ALL | E_STRICT); // NOT FOR PRODUCTION SERVERS!
// @ini_set('display_errors', '1');    // NOT FOR PRODUCTION SERVERS!
// $CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
// $CFG->debugdisplay = 1;             // NOT FOR PRODUCTION SERVERS!

require_once(__DIR__ . '/lib/setup.php');


/*start::for debuging */
// $CFG->debug = E_ALL;
// $CFG->debugdisplay = 1;
// $CFG->langstringcache = 0;
// $CFG->cachetemplates = 0;
// $CFG->cachejs = 0;
// $CFG->perfdebug = 15;
// $CFG->debugpageinfo = 1; 
/*end::for debuging
// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
*/
