<?php 
@error_reporting(E_ALL | E_STRICT);
@ini_set('display_errors', '1');
session_start();
require(__DIR__.'/../config.php');
require_once($CFG->dirroot . '/user/editlib.php');
require_once($CFG->libdir . '/authlib.php');
require_once('lib.php');
$PAGE->set_url('/login/mobile_reg.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_title("OTP Based Rewgistration");
$PAGE->set_heading($SITE->fullname);
echo $OUTPUT->header();

global $DB;
//echo 'hi';
$sql = "SELECT id,password,username FROM {user}";
$db_result = $DB->get_record_sql($sql);
//print_r($db_result);
$message = "";
if(isset($_POST['submitForm'])){ //check if form was submitted
    
    echo $message = "Success! ".$mobile;
    $user = new stdClass();

    $user->username = $_POST['username'];
    $user->phone1 = $_POST['mobile'];
    $user->password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $user->confirmed = 1;
    $user->mnethostid = 1;

    $insert_id = $DB->insert_record('user', $user);

    if($insert_id){
        echo "<h1>User created successfully..</h1>";
    }else{
        echo "Something went wrong";
    }
   
  } 
if(isset($_POST['BtnVerifyOtp'])){ 
    $otp = $_SESSION['otp'];    
    $votp = $_POST['votp'];
    echo $otp.'--'.$votp;
    if($otp == $votp){
        echo "Mobile No verified..";
    }else{
        echo "Mobile No NOT verified..";
    }
} 

?>
        <script>
        function getOTP() {
            var mbl = $('#mobile').val();
            //console.log('--'+mbl);
            $.ajax({
                url:"sendOTP.php",    
                type: "post",
                data: {mobile: mbl},
                success:function(res){
                    console.log(res);
                    res = JSON.parse(res);
                    console.log(res.result.status);
                    if(res.result.status != 0){
                        console.log(res.result.error.mobile);
                        if(res.result.error.mobile != ''){
                            $('#id_error_mobile').html(res.result.error.mobile);
                            $('#id_error_mobile').show();
                            $('#mobile').addClass("is-invalid");
                        }
                    }else{
                        console.log(res.result.message);
                        var otp = res.result.otp;
                        sessionStorage.setItem("otp", otp);
                        var sotp = sessionStorage.getItem("otp"); 
                        $("#BtnVotp").removeClass("disable");
                        $('#id_error_mobile').html(res.result.message);
                        $('#id_error_mobile').show();
                        $('#mobile').removeClass("is-invalid");
                        console.log(otp+'----'+sotp);
                    }
                    
                }
            });
        }
        function verifyOTP() {
            var votp = $('#votp').val();
            var sotp = sessionStorage.getItem("otp");
            if(votp === sotp){
                console.log("1");
                $("#submitForm").removeAttr('disabled');
                $('#id_error_votp').show();
                $('#id_error_votp').html("Mobile Number Verified Successfully");
            }else{
                $('#id_error_votp').show();
                $('#id_error_votp').html("OTP Not Matched");
            }
            //console.log(votp+'++++'+sotp);
        }
        </script>
        <style>
            .disable{
                pointer-events: none;
                cursor:default;
            }
        </style>
    
        
            <div class="fcontainer clearfix">
            <div class="row justify-content-md-center" >
            <div class="col-md-8 col-xl-6">
            <div class="card">
            <div class="card-body">
                <form action="" method="post">
                    <div class="form-group row fitem has-danger">
                        <div class="col-md-3">
                            <span class="float-sm-right text-nowrap">
                                <abbr class="initialism text-danger" title="Required"><i class="icon fa fa-exclamation-circle text-danger fa-fw " title="Required" aria-label="Required"></i></abbr>
                            </span>
                            <label class="col-form-label d-inline " for="id_mobile">Mobile Number </label>
                        </div>
                        <div class="col-md-5">
                            <input type="text" class="form-control" size="25" name="mobile" id="mobile" maxlength="10" pattern="[1-9]{1}[0-9]{9}" aria-describedby="id_error_mobile" aria-invalid="false">
                            <div class="form-control-feedback invalid-feedback" id="id_error_mobile" tabindex="0"></div>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="btn btn-primary" onclick="return getOTP();">Get OTP</a>
                        </div>
                    </div>
                    <div class="form-group row fitem has-danger">
                        <div class="col-md-3">
                            <span class="float-sm-right text-nowrap">
                                <abbr class="initialism text-danger" title="Required"><i class="icon fa fa-exclamation-circle text-danger fa-fw " title="Required" aria-label="Required"></i></abbr>
                            </span>
                            <label class="col-form-label d-inline " for="id_votp">Verify OTP </label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" size="25" maxlength="4" name="votp" id="votp" aria-describedby="id_error_votp" aria-invalid="false">
                            <div class="form-control-feedback invalid-feedback" id="id_error_votp" tabindex="0"></div>
                        </div>
                        <div class="col-md-3">
                        <a href="#" id="BtnVotp" onclick="return verifyOTP();" class="disable btn btn-primary"> Verify OTP</a>
                        </div>
                    </div>
                    
                    <div class="form-group row fitem has-danger">
                        <div class="col-md-3">
                            <span class="float-sm-right text-nowrap">
                                <abbr class="initialism text-danger" title="Required"><i class="icon fa fa-exclamation-circle text-danger fa-fw " title="Required" aria-label="Required"></i></abbr>
                            </span>
                            <label class="col-form-label d-inline " for="id_username">Username </label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" size="25" name="username" id="username" aria-describedby="id_error_username" aria-invalid="false">
                            <div class="form-control-feedback invalid-feedback" id="id_error_username" tabindex="0"></div>
                        </div>
                    </div>
                    
                    <div class="form-group row fitem has-danger">
                        <div class="col-md-3">
                            <span class="float-sm-right text-nowrap">
                                <abbr class="initialism text-danger" title="Required"><i class="icon fa fa-exclamation-circle text-danger fa-fw " title="Required" aria-label="Required"></i></abbr>
                            </span>
                            <label class="col-form-label d-inline " for="id_password">Password </label>
                        </div>
                        <div class="col-md-4">
                            <input type="password" class="form-control" size="25" name="password" id="password" aria-describedby="id_error_password" aria-invalid="false">
                            <div class="form-control-feedback invalid-feedback" id="id_error_password" tabindex="0"></div>
                        </div>
                    </div>
                    <div id="fgroup_id_buttonar" class="form-group row  fitem femptylabel" data-groupname="buttonar">
                        <div class="col-md-3">
                            <span class="float-sm-right text-nowrap"></span>
                        </div>
                        <div class="col-md-9">
                            <input type="submit"  class="btn btn-primary" value="Register" name="submitForm" id="submitForm" disabled>                    
                        </div>
                    </div>
                </form>   
            </div>
            </div>
            </div>
            </div>
            </div>
        

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<?php echo $OUTPUT->footer(); ?>