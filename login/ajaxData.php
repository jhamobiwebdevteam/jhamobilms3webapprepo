<?php 
// @error_reporting(E_ALL | E_STRICT);
// @ini_set('display_errors', '1');

require('../config.php');
global $DB; 
if(!empty($_POST["schoolid"])){ 
    $schoolid = $_POST["schoolid"];
    if(strlen($schoolid) == 11){
        $getId = $DB->get_record_sql("SELECT id,name FROM {course_categories} WHERE parent = 0 AND idnumber LIKE '%$schoolid%'");    
        if(!empty($getId)){ 
            $id = $getId->id; 
            $school_name = $getId->name; 
            $params = array('parent' => $id, 'visible' => 1);
            $getCategory = $DB->get_records('course_categories', $params, 'id', 'id,name'); 
            $getCategory = array_values( $getCategory);
            //print_r($getCategory);
        //  die('hi'.count($getCategory));
            if(count($getCategory) > 0){ 
                $res[] =  '<option value="">Select Year</option>'; 
                foreach ($getCategory as $rec) {
                    $res[]=  '<option value="'.$rec->id.'">'.$rec->name.'</option>'; 
                } 
            }else{ 
                $res[] = '<option value="">Year not available</option>'; 
            }
            $result['result']['school_name'] = $school_name;
            $result['result']['data'] = $res;
            $result['result']['categoryList'] = $getCategory;
            $result['result']['status'] = 1;
            echo json_encode($result);
        }else{ 
            $result['result']['status'] = 0;
            $result['result']['msg'] = "For School ID Please Connect Your School";
            echo json_encode($result);
        }
    }else{
        $result['result']['status'] = 0;
        $result['result']['msg'] = "Please enter 11 digit school ID";
        echo json_encode($result);
    }    
}else if(!empty($_POST["yearid"])){ 
    $yearid = $_POST["yearid"];
    $params = array('parent' => $yearid);
    $getCategory = $DB->get_records('course_categories', $params, 'id', 'id,name'); 
    //print_r($getCategory);
//  die('hi'.count($getCategory));
    if(count($getCategory) > 0){ 
        $res[] =  '<option value="">Select Grade</option>'; 
        foreach ($getCategory as $rec) {
            $res[]=  '<option value="'.$rec->id.'">'.$rec->name.'</option>'; 
        } 
    }else{ 
        $res[] = '<option value="">Grade not available</option>'; 
    }
    $result['result']['school_name'] = $school_name;
    $result['result']['data'] = $res;
    $result['result']['status'] = 1;
    echo json_encode($result);
}else if(!empty($_POST["gradeid"])){ 
    $gradeid = $_POST["gradeid"];   
    $params = array('parent' => $gradeid);
    $getCategory = $DB->get_records('course_categories', $params, 'id', 'id,name'); 
    //print_r($getCategory);
//  die('hi'.count($getCategory));
    if(count($getCategory) > 0){ 
        $option[] = '<option value="">Select Section</option>'; 
        foreach ($getCategory as $rec) {            
            $option[] = '<option value="'.$rec->id.'">'.$rec->name.'</option>';             
        }  
        $result['result']['msg'] = $option;
        $result['result']['status'] = 1;
        echo json_encode($result);
       // die('hi');
    }else{ 
        $params = array('id' => $gradeid);
        $getCnt = $DB->get_record('course_categories', $params, 'coursecount');
        if($getCnt->coursecount != 0){          
           // die('-'.$gradeid);  
            $params1 = array('category' => $gradeid);
            $getCourse = $DB->get_records('course', $params1, 'id', 'id,fullname'); 
            foreach ($getCourse as $rec) {            
                $course['ids'][] = $rec->id;             
                $course['name'][] = $rec->fullname;             
            }
            $result['result']['course'] = $course;
            $result['result']['msg'] = "Available course list";
            $result['result']['status'] = 2;
            echo json_encode($result);            
        }else{
            $result['result']['msg'] = "Course NOT available";
            $result['result']['status'] = 0;
            echo json_encode($result);
        }
    } 
}
else if(!empty($_POST["sectionid"])){ 
    $sectionid = $_POST["sectionid"];     
    $getPath = $DB->get_record_sql("SELECT path FROM {course_categories} WHERE id = '$sectionid'");    
    $params1 = array('category' => $sectionid);  
    $getCourse = $DB->get_records('course', $params1, 'id', 'id,fullname'); 
    if(!empty($getCourse)){  
        foreach ($getCourse as $rec) {            
            $course['ids'][] = $rec->id;             
            $course['name'][] = $rec->fullname;             
        }
        $result['result']['course'] = $course;
        $result['result']['path'] = $getPath->path;
        $result['result']['msg'] = "Available course list";
        $result['result']['status'] = 1;
        echo json_encode($result);            
    }else{
        $result['result']['msg'] = "Course NOT available";
        $result['result']['status'] = 0;
        echo json_encode($result);
    }
     
}
else if(!empty($_POST["uname"])){ 
   
    $uname = $_POST["uname"];   
    $params1 = array('username' => $uname);
    
    $getuname = $DB->get_record('user', $params1, 'id', 'id');     
    if(!empty($getuname)){         
        $result['result']['msg'] = "Username Already Exist";
        $result['result']['status'] = 0;
        echo json_encode($result);        
    }else{
        $result['result']['msg'] = "Username validated";
        $result['result']['status'] = 1;
        echo json_encode($result);
    }
     
}
?>