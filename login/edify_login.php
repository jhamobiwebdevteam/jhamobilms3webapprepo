<?php
require_once('../config.php');
global $USER, $DB, $OUTPUT, $CFG, $PAGE, $SITE, $SESSION;

if(isloggedin()){
    return redirect($CFG->wwwroot.'/my');
}
$token = \core\session\manager::get_login_token();
$url = $CFG->wwwroot . "/login/index.php";

$username = isset($_POST['username'])?$_POST['username']:'';
$password = isset($_POST['password'])?$_POST['password']:'';
if(empty($username) || empty($password)){
    return redirect($CFG->wwwroot.'/login/index.php');
}

$PAGE->set_context(context_system::instance());
echo $OUTPUT->header();
$courseurl = $CFG->wwwroot . "/my/";
?>

<form action="<?=$url?>" method="post">
<input type="hidden" name="username" value="<?=$username?>">
<input type="hidden" name="password" value="<?=$password?>">
<input type="hidden" name="logintoken" value="<?= $token ?>">
<script>
    $(document).ready(function(){
        $('form').submit();
    });
</script>
</form>

<?php 
echo $OUTPUT->footer();
?>

