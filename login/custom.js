require(['core/first','jquery','jqueryui','core/ajax'],function(core, $, bootstrap, ajax){
    $('<style>.disable { pointer-events : none ; cursor : default; } #fitem_id_other{display:none} #fitem_id_intrestedAreas{margin-bottom: 0;}</style>').appendTo('body');
    // $('#id_submitbutton').addClass('disable');  
    $('#id_other').prop('required',false);
    $(document).ready(function(){
        var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';
        console.log(base_url);
        $(document).on('change','#id_username',function(){
            var uname = $('#id_username').val();
            
            // var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            // if( !testEmail.test(email)) {
            //     $('#id_error_username').html('Please enter valid Email');
            //     $('#gotp').addClass('disable');
            //     $('#votp').addClass('disable');
            //     $('#id_submitbutton').addClass('disable');
            //     $('#id_error_username').show();
            //     $('#id_username').addClass("is-invalid");
            // } else {
                //$('#gotp').removeClass('disable');
                // $('#id_error_username').hide();
                // $('#id_username').removeClass("is-invalid");
                $.ajax({
                    type:'POST',
                    url:base_url+'login/ajaxData.php',
                    data:'uname='+uname,
                    success:function(res){
                        res = JSON.parse(res);
                        if(res.result.status == 1){     
                            $('#id_error_username').hide();
                            $('#id_username').removeClass("is-invalid");  
                            $('#id_error_username').html("<span style='color:green;'>" +res.result.msg+ "</span>");  
                            // $('input[name="email"]').val(email);
                            // $('input[name="email2"]').val(email);
                        }else{
                            $('#id_username').addClass("is-invalid");
                            $('#id_error_username').show();
                            $('#id_error_username').html(res.result.msg);  
                        }               
                    }  
                });
            //}            
        });

       $(document).on('click','#gotp',function(){            
            var mbl = $('#id_phone2').val();
            $.ajax({
                url:base_url+"/login/sendOTP.php",
                type: "post",
                data: {mobile: mbl},
                success:function(res){
                    res = JSON.parse(res);
                    //console.log(res.result.status);
                    if(res.result.status == 0){
                         var otp = res.result.otp;
                         sessionStorage.setItem("sotp", otp);
                         //var sotp = sessionStorage.getItem("sotp"); 
                        $("#votp").removeClass("disable");
                        $('#id_error_phone2').html("<span style='color:green;'>" +res.result.message+ "</span>");
                        $('#id_error_phone2').show();
                        $('#id_phone2').removeClass("is-invalid");   
                        $("#id_phone2").focus();         
                    }else{
                        $('#id_error_phone2').html(res.result.message);
                        $('#id_error_phone2').show();
                        $('#id_phone2').addClass("is-invalid");
                        $("#id_phone2").focus();
                    }
                }
            });  
       });

       $(document).on('click','#votp',function(){   
            var votp = $('#id_votp').val();
            var sotp = sessionStorage.getItem("sotp");                 
            //console.log(votp+'----'+sotp);       
            if(votp === sotp){
                //$("#id_submitbutton").removeClass('disable');
                $('#id_error_votp').show();
                $('#id_error_votp').html("<span style='color:green;'>Mobile Number Verified Successfully </span>");
                $("#id_votp").focus();   
                $('#id_phone2').addClass("disable");  
                $('#id_votp').addClass("disable");  
                $('#gotp').addClass("disable");  
                $('#votp').addClass("disable");  
            }else{
                $('#id_error_votp').show();
                $('#id_error_votp').html("OTP Not Matched");
            }
        });
        
        $(document).on('change','#id_schoolid',function(){ 
            var schoolid = $('#id_schoolid').val();
            $.ajax({
                type:'POST',
                url:base_url +'/login/ajaxData.php',
                data:'schoolid='+schoolid,
                success:function(res){
                    res = JSON.parse(res);
                   //console.log(res);
                    if(res.result.status != 1){
                        $('#id_schoolid').addClass("is-invalid");
                        $('#id_error_schoolid').show();
                        $('#id_error_schoolid').html(res.result.msg);                            
                    }else{
                        // $('#id_year').html(res.result.data);
                        // $('#id_year').html(res.result.data);
                        $("select#id_year").html('');
                        $("<option>").val('').html("Select year").appendTo("select#id_year"); 
                        var categoryList =    res.result.categoryList;
                        for(var i=0; i< categoryList.length;i++){
                           // var selectedCourse = location.queryString.id==response[i].id?' selected ':'';
                            $("<option>").val(categoryList[i].id).html(categoryList[i].name).appendTo("select#id_year");
                        }


                        $('#id_error_schoolid').show();
                        $('#id_error_schoolid').html(res.result.school_name);
                        $('input[name="schoolname"]').val(res.result.school_name);
                        $('#id_schoolid').removeClass("is-invalid");
                        $('<style> #id_error_schoolid{display:block} </style>').appendTo('#id_error_schoolid');
                        //$('#id_error_schoolid').hide();
                    }
                }
            });            
       });

        $(document).on('change','#id_year',function(){ 
            var yearid = $('#id_year').val();
            var yearname = $("#id_year :selected").text();
            $('input[name="years"]').val(yearname);
            $.ajax({
                type:'POST',
                url:base_url +'/login/ajaxData.php',
                data:'yearid='+yearid,
                success:function(res){
                    res = JSON.parse(res);
                   //console.log(res);
                    if(res.result.status != 1){
                        $('#id_year').addClass("is-invalid");
                        $('#id_error_year').show();
                        $('#id_error_year').html(res.result.msg);                            
                    }else{
                        $('#id_grade').html(res.result.data);
                        $('#id_error_year').hide();                        
                        $('#id_year').removeClass("is-invalid");
                    }
                }
            });            
       });

       $(document).on('change','#id_grade',function(){ 
            $("#id_section").html("");
            $('input[name="sections"]').val("");
            var gradeid = $('#id_grade').val();
            var gradename = $("#id_grade :selected").text();
            $('input[name="grades"]').val(gradename);
            $.ajax({
                type:'POST',
                url:base_url +'/login/ajaxData.php',
                data:'gradeid='+gradeid,
                success:function(res){
                    res = JSON.parse(res);
                    if(res.result.status == 1){                              
                        $('#id_section').html(res.result.msg);
                    }else if(res.result.status == 2){         
                        //$('#crsList').html(res.result.course);    
                        $('#crsList').html(res.result.msg+ ' '+res.result.course.name);      
                        $('input[name="csids"]').val(res.result.course.ids);   
                        $('#id_submitbutton').removeClass("disable");               
                        //$('#id_section').html(res.result.msg);
                    }else{
                        $('#id_section').addClass("is-invalid");
                        $('#id_error_section').show();
                        $('#id_error_section').html(res.result.msg);  
                        $('input[name="sections"]').val('');
                    }               
                }  
            });            
        });

       $(document).on('change','#id_section',function(){            
            var sectionid = $('#id_section').val();
            if(sectionid != ''){
                var section = $("#id_section :selected").text();
                $('input[name="sections"]').val(section);
            }            
            $.ajax({
                type:'POST',
                url:base_url +'/login/ajaxData.php',
                data:'sectionid='+sectionid,
                success:function(res){
                    res = JSON.parse(res);
                   
                    if(res.result.status == 1){     
                        $('#id_submitbutton').removeClass("disable");   
                        $('#crsList').html(res.result.msg+ ' '+res.result.course.name);      
                        $('input[name="csids"]').val(res.result.course.ids);      
                        $('input[name="section_path"]').val(res.result.path);      
                    }else{
                        $('#id_section').addClass("is-invalid");
                        $('#id_error_section').show();
                        $('#id_error_section').html(res.result.msg);  
                    }               
                }  
            });              
        });

       $(document).on('keyup','#id_phone2',function(){
            var phone = $('#id_phone2').val();
            if(!$.isNumeric(phone)) {
                $('#id_phone2').val('');
                $('#id_phone2').addClass("is-invalid");
                $('#id_error_phone2').html('Please enter only Number');
                $('#id_error_phone2').show();
                $('#id_error_phone2').addClass("is-invalid");
            }else{
                if(phone.length == 10){
                    $('#gotp').removeClass('disable');
                }else{ 
                    $('#gotp').addClass('disable');
                }               
                $('#id_error_phone2').hide();
                $('#id_error_phone2').addClass("valid");
                $('#id_phone2').removeClass("is-invalid");
            } 
       });

       $('#id_intrested_area_Others').click(function(){
            if($(this).is(':checked')){
                $("#fitem_id_other").css({ display: "flex" });
                $('#id_other').prop('required',true);
            } else {
                $("#fitem_id_other").css({ display: "none" });
                $('#id_other').prop('required',false);
            }
        });
      
       
    });
});