
require(['core/first','jquery','jqueryui','core/ajax'],function(core, $, bootstrap, ajax){

$(document).ready(function(){
           console.log('custome.js loded');
    var chapter = 0;
    $('table.generaltable.user-grade tbody tr').each(function(){
            let $tr = $(this);
            
          
            if($tr.find('.level1').length){
                $tr.addClass('level1-tr');
            }else if($tr.find('.level2').length){
                $tr.addClass('level2-tr');
                if($tr.find('td.column-leader').length){
                    chapter++;
                    $tr.addClass('chapter-'+chapter+'-header');
                }
                if($tr.find('td.baggb').length){
                    $td = $tr.find('th.baggb.column-itemname');
                    $td.append("<a href='javascript:void(0);' datachapter='"+chapter+"' class='fa fa-caret-right' style='margin-left:15px;color:black;font-size:20px;'></a>");
                    $tr.addClass('chapter-'+chapter+'-total');
                }
            }else if($tr.find('.level3').length){
                $tr.addClass('level3-tr');
                $tr.addClass('chapter-'+chapter+'-item');
            }

            if($tr.find('.column-leader').length){
               let rowspan =  $tr.find('td.column-leader').prop("rowspan");

               
               let $td = $tr.find('td.column-leader');
               $td.attr("datarowspan",rowspan);
            }
        });

        $('table.generaltable.user-grade tbody .level2.baggb.column-itemname a').each(function(index){
            $('table.generaltable.user-grade tbody .level2.baggb.column-itemname a')[index].click();
        });


   });

   $(document).on('click','.level2.baggb.column-itemname a',function(){
       
        let chapter = $(this).attr('datachapter');
        let show = $(this).hasClass('fa-caret-right')
         let $tbody = $(this).closest('tbody'); 
         $(this).toggleClass('fa-caret-down fa-caret-right');
         if(show){
            $tbody.find('tr.chapter-'+chapter+'-item').hide(); 
         }else{
            $tbody.find('tr.chapter-'+chapter+'-item').show(); 
         }
         let chapter_row_visible_row_count = $tbody.find('tr:visible[class*="chapter-'+chapter+'-"]').length;
         let all_visible_row_count = $tbody.find('tr:visible').length;
         $tbody.find('tr.chapter-'+chapter+'-header td.column-leader').prop("rowspan",chapter_row_visible_row_count);
         $tbody.find('tr td.level1.column-leader').prop("rowspan",all_visible_row_count);

    });
});
