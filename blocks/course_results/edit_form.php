<?php

class block_course_results_edit_form extends block_edit_form {
    /**
     * @param MoodleQuickForm $mform
     */	
	protected function specific_definition($mform) {
		global $CFG;
		require_once($CFG->dirroot . '/grade/lib.php');
		
		//header
		$mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));
				
        $options = array();
        $items = grade_item::fetch_all(array('courseid'=>$this->page->course->id));
        foreach($items as $key=>$item) {
            switch($item->itemtype) {
                case 'category':
                    $options[$key] = get_string('categorytotal', 'grades') . ': ' . $item->get_item_category()->fullname;
                    break;
                case 'course':
                    $options[$key] = get_string('coursetotal', 'grades');
                    break;
                default:
                    $options[$key] = $item->itemname;
            }
        }
		$mform->addElement('select', 'config_itemid', get_string('config_select_item', 'block_course_results'), $options);		
		$mform->setDefault('config_select_item', empty($this->config->itemid) ? 0 : $this->config->itemid);
		
		$mform->addElement('text', 'config_showbest', get_string('config_show_best', 'block_course_results'));
		$mform->addElement('text', 'config_showworst', get_string('config_show_worst', 'block_course_results'));

        $mform->addElement('selectyesno', 'config_usegroups', get_string('config_use_groups', 'block_course_results'));
        $mform->setDefault('config_usegroups', 1);
		
		$usenames = array(B_COURSERESULTS_NAME_FORMAT_FULL => get_string('config_names_full', 'block_course_results'), B_COURSERESULTS_NAME_FORMAT_ID => get_string('config_names_id', 'block_course_results'), B_COURSERESULTS_NAME_FORMAT_ANON => get_string('config_names_anon', 'block_course_results'));
		$mform->addElement('select', 'config_nameformat', get_string('config_name_format', 'block_course_results'), $usenames);

		$formats = array(B_COURSERESULTS_GRADE_FORMAT_PCT => get_string('config_format_percentage', 'block_course_results'), B_COURSERESULTS_GRADE_FORMAT_FRA => get_string('config_format_fraction', 'block_course_results'), B_COURSERESULTS_GRADE_FORMAT_ABS => get_string('config_format_absolute', 'block_course_results'));
		$mform->addElement('select', 'config_gradeformat', get_string('config_grade_format', 'block_course_results'), $formats);
		
		$mform->addElement('text', 'config_blocktitle', get_string('config_title', 'block_course_results'));
		$mform->setType('config_blocktitle', PARAM_MULTILANG);
		
		$mform->addElement('selectyesno', 'config_showuserpic', get_string('config_showuserpic', 'block_course_results'));
		
		$mform->addElement('editor', 'config_blockheader', get_string('config_header', 'block_course_results'));
		$mform->setType('config_blockheader', PARAM_RAW);
		$mform->addElement('editor', 'config_blockfooter', get_string('config_footer', 'block_course_results'));
		$mform->setType('config_blockfooter', PARAM_RAW);				
		
    }
 }