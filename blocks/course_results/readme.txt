Course Results block by Vlas Voloshin and mko_san
This is a ported version of Course Results from 1.9 to 2.0+ (tested with 2.1)

Original text:
Course Results block by Vlas Voloshin
argentum@cdp.tsure.ru

This course block shows information about highest grades in a course on some grade item.
It was created on the base of quiz results block, but instead of quiz results it shows information about any grades in a course: course item grades, grade category results or whole course grades.
Block title, header and footer can be changed in settings as well as grade item to show and other settings.
You can also choose whether to show user pictures thumbnails near their names in block's settings. User pictures WON'T be shown, if the block is configurated to show group results or if the users are shown anonymously. 

Installation:
Unzip to /moodle/blocks/, then go to admin notification page to finish installation.
Add it to course page like any other block, and don't forget to configure it.

If you have any bugs to report or extra functionality to suggest, you can use tracker.moodle.org: just create an issue in "Non-core contributed modules" with "Block: Course results" component.