<?PHP
$string['pluginname'] = 'Wyniki';
$string['pluginadministration'] = 'Wyniki administration';
$string['bestgrade'] = 'Najlepszy wynik:';
$string['bestgrades'] = '{$a} najlepszych wyników:';
$string['bestgroupgrade'] = 'Grupa z najwyższą średnią:';
$string['bestgroupgrades'] = '{$a} group z najwyższymi średnimi:';
$string['config_format_absolute'] = 'Wartości bezwzględne';
$string['config_format_fraction'] = 'Ułamki';
$string['config_format_percentage'] = 'Procenty';
$string['config_grade_format'] = 'Wyświetlaj wyniki jako:';
$string['config_name_format'] = 'Poziom prywatności wyników:';
$string['config_names_anon'] = 'Anonimowe';
$string['config_names_full'] = 'Imię i Nazwisko';
$string['config_names_id'] = 'Tylko numery ID';
$string['config_select_item'] = 'Wyświetlaj wyniki z';
$string['config_show_best'] = 'Ile najwyższych wyników wyświetlić (0 żeby wyłączyć)?';
$string['config_show_worst'] = 'Ile najniższych wyników wyświetlić (0 żeby wyłączyć)?';
$string['config_use_groups'] = 'Pokaż grupy zamiast studentów (mozliwe tylko jeśli dany quiz zezwala na grupy)?';
$string['configuredtoshownothing'] = 'Blok nieskonfigurowany. Skonfiguruj lub ukryj ten blok.';
$string['error_emptyitemid'] = 'Błąd: nie wybrano źródła ocen w konfiguracji.';
$string['error_emptyitemrecord'] = 'Błąd: wybrane źrodło ocen już nie istnieje. Wybierz inne źródło';
$string['error_nogroupsexist'] = 'Błąd: wybrano wyświetlnie wyników grupami ale nie ma grup zdefiniownych w kursie.';
$string['formaltitle'] = 'Wyniki';
$string['worstgrade'] = 'Najgorszy wynik:';
$string['worstgrades'] = '{$a} najgorszych wyników:';
$string['worstgroupgrade'] = 'Grupa z najniższą średnią:';
$string['worstgroupgrades'] = '{$a} group z najniższymi średnimi:';
$string['config_showuserpic'] = 'Pokaż awatary';
$string['config_title'] = 'Tytuł bloku';
$string['config_header'] = 'Nagłówek bloku';
$string['config_footer'] = 'Stopka bloku';

?>