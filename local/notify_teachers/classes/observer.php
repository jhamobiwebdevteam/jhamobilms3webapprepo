<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used by the weeks course format.
 *
 * @package format_weeks
 * @copyright 2017 Mark Nelson <markn@moodle.com>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for format_weeks.
 *
 * @package format_weeks
 * @copyright 2017 Mark Nelson <markn@moodle.com>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_notify_teachers_observer {
    /**
     * Triggered via \core\event\course_section_created event.
     *
     * @param \core\event\course_section_created $event
     */
    public static function course_module_created(\core\event\course_module_created $event) {
		GLOBAL $DB,$CFG;
	
		$fromuser = $DB->get_record('user',array('id'=>2));
		$touser = $DB->get_record('user',array('id'=>2));
		
		// echo $event->other['name'];
		// print_r($event);die;
		
		if($event->courseid != ""){
			$course = $DB->get_record('course',array('id'=>$event->courseid),'fullname');
			
			$eventdata = new \core\message\message();
			$eventdata->courseid          = $event->courseid;
			$eventdata->component         = 'moodle';
			$eventdata->name              = "insights";
			$eventdata->userfrom          = $fromuser;
			$eventdata->userto            = $touser;
			$eventdata->subject           = "Neww activity added";
			$eventdata->fullmessage       = "A new activity has been added in courses <button href='".$CFG->wwwroot."'>Go to course</button>";
			$eventdata->fullmessageformat = FORMAT_HTML;
			$eventdata->fullmessagehtml   = "A neww activity has been added in <a href='".$CFG->wwwroot."/course/view.php?id=".$event->courseid."'>".$course->fullname."</a>";
			$eventdata->smallmessage      = "A neww activity has been added in courses <button href='".$CFG->wwwroot."'>Go to course</button>";
			$eventdata->notification      = 1;
			$eventdata->contexturl      = $CFG->wwwroot."/course/view.php?id=".$event->courseid;
			$send = message_send($eventdata);
			
			// ADD activity to popup message table
			if($send){
				$record = new stdClass();
				$record->notificationid = $send;

				$DB->insert_record('message_popup_notifications', $record);
			}
			
		}
		
		
		
    }
 
}
