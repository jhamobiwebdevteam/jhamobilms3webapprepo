<?php 

defined('MOODLE_INTERNAL') || die();

$observers = array(
    array(
        'eventname'   => 'core\event\course_module_created',
        'callback'    => 'local_notify_teachers_observer::course_module_created',
    )
);