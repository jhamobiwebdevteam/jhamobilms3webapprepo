<?php
header('Content-type: application/json');
require(__DIR__.'/../../config.php');
///local/ajax/change-password.php
$result = [];

//echo password_verify('User5@123456', '$2y$10$SPf5O/u73MpbSPD.IxEoF.TaNUS1KSZZjIGspU/OhSLUJ25Kj3vhe');
//user1 $2y$10$AXPBDTIWxGiXkn3VgfVfN.UJW0X0iXTc84x46oEjL/db9q1N9tKfa User1@123456
//password_verify($requestData['password'], $user['password'])

if(!isset($_POST['user_id'])||empty($_POST['user_id'])){
    $result['result']['error']="User id is missing.";
    
    echo json_encode($result);
    die;
}

if(!isset($_POST['current_password'])||empty($_POST['current_password'])){
    $result['result']['error']="Please enter current password.";
    echo json_encode($result);
    die;
}
if(!isset($_POST['new_password'])||empty($_POST['new_password'])){
    $result['result']['error']="Please enter new password.";
    echo json_encode($result);
    die;
}

if(!isset($_POST['confirm_password'])||empty($_POST['confirm_password'])){
    $result['result']['error']="Please enter confirm password.";
    echo json_encode($result);
    die;
}

if(strcmp($_POST['new_password'], $_POST['confirm_password'])!=0){
    $result['result']['error']="Please confirm password.";
    echo json_encode($result);
    die;
}


if(isset($_POST['user_id']) && isset($_POST['current_password']) && isset($_POST['new_password']) && isset($_POST['confirm_password'])){
    $user_id = $_POST['user_id'];
    //get user details
    $sql = "SELECT id,password FROM {user} WHERE id = $user_id";
    $db_result = $DB->get_record_sql($sql);
    
     if(!password_verify($_POST['current_password'],$db_result->password)){
          $result['result']['error']="Invalid current password.";
          echo json_encode($result);
          die;
     }
     
    if($DB->set_field('user', 'password', password_hash($_POST['new_password'], PASSWORD_BCRYPT), ['id'=>$user_id ])){
        $result['result']['success']="Password changed successfully.";
          echo json_encode($result);
          die;
    }
    
}

 $result['result']['error']="Something went wrong.";
          echo json_encode($result);
          die;
