<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$functions = array(
    'log_ajax_getcourselist'=>array(
        'classname'=>'local_ajax_external',
        'methodname'=>'getcourseincategory',
        'classpath'=>'local/ajax/externallib.php',
        'description'=>'Return course list in category',
        'type'=>'read',
        'ajax'=>true,
        'capabilities'=>'',
        'loginrequired'=>true,
    ),
    'log_ajax_getuserlist'=>array(
        'classname'=>'local_ajax_external',
        'methodname'=>'getuserlist',
        'classpath'=>'local/ajax/externallib.php',
        'description'=>'Return enrolled user list',
        'type'=>'read',
        'ajax'=>true,
        'capabilities'=>'',
        'loginrequired'=>true,
    ),
    'log_ajax_getvideourl'=>array(
        'classname'=>'local_ajax_external',
        'methodname'=>'getvideo_url',
        'classpath'=>'local/ajax/externallib.php',
        'description'=>'Return video url',
        'type'=>'read',
        'ajax'=>true,
        'capabilities'=>'',
        'loginrequired'=>true,
    ),
    'getmobileversion'=>array(
        'classname'=>'local_ajax_external',
        'methodname'=>'getMobileVersion',
        'classpath'=>'local/ajax/externallib.php',
        'description'=>'Return mobile version and version type',
        'type'=>'read',
        'ajax'=>true,
        'capabilities'=>'',
        'loginrequired'=>false,
    ),
);