<?php

/*
 * Return mobile app version 
 * local/ajax/mobile_app_version.php
 */

require(__DIR__.'/../../config.php');

header('Content-Type: application/json');
 global $DB;
        $sql ="
                 SELECT version_code, version_type FROM {mobile_app_version} ORDER BY `mdl_mobile_app_version`.`version_code`  DESC LIMIT 1
                     ";
          $db_result = $DB->get_record_sql($sql);

echo json_encode(['response'=>$db_result]);