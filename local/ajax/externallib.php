<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//require(__DIR__.'/../../config.php');
require_once($CFG->libdir . "/externallib.php");

class local_ajax_external extends external_api
{

    /* Return descriptio of method parameters
     * @return external_function_parameters
     */
    public static function getcourseincategory_parameters()
    {
        return new external_function_parameters(
            array("category_id" => new external_value(PARAM_RAW, "category_id"))
        );
    }
    public static function getcourseincategory_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'settings content text'),
                    'fullname' => new external_value(PARAM_RAW, 'settings content text'),
                )
            )
        );
    }
    public static function getcourseincategory($id)
    {
        global $DB, $USER, $CFG;
        $params = self::validate_parameters(self::getcourseincategory_parameters(), array('category_id' => $id));

        //        $role = $DB->get_records('role',array('shortname'=>'teacher'));
        //        $context = CONTEXT_COURSE::instance($id);
        //        $teachers = get_role_users($role->id, $context, true);
        $result = self::get_course_id_list($id);
        return $result;
    }

    public static function getuserlist_parameters()
    {
        return new external_function_parameters(
            array(
                "category_id" => new external_value(PARAM_RAW, "category_id"),
                "course_id" => new external_value(PARAM_RAW, "course_id")
            )
        );
    }
    public static function getuserlist_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'settings content text'),
                    'firstname' => new external_value(PARAM_RAW, 'settings content text'),
                    'lastname' => new external_value(PARAM_RAW, 'settings content text'),
                )
            )
        );
    }
    public static function getuserlist($categoryId, $courseId)
    {
        global $DB, $USER, $CFG;
        $params = self::validate_parameters(
            self::getuserlist_parameters(),
            array('category_id' => $categoryId, 'course_id' => $courseId)
        );

        //        $role = $DB->get_records('role',array('shortname'=>'teacher'));
        //        $context = CONTEXT_COURSE::instance($id);
        //        $teachers = get_role_users($role->id, $context, true);
        $result = self::get_enrolled_user_list($categoryId, $courseId);
        return $result;
    }


    public function get_course_id_list($category_id)
    {
        global $DB;
        if ($category_id == '') {
            $category_id_list = array();
            $sql = "SELECT id,fullname FROM {course} WHERE 1 ";
        } else {
            $category_id_list = self::get_child_category_id($category_id);
            $sql = "SELECT id,fullname FROM {course} WHERE category IN (" . implode(',', $category_id_list) . ") ";
        }

        //$result = $DB->get_fieldset_sql("SELECT id FROM {course} WHERE category IN (". implode(',', $category_id_list).") ");


        // $paramsDB = $params; //array($itemid);
        $db_result = $DB->get_records_sql($sql);

        return $db_result;
        //return $result;
    }

    public function get_child_category_id($parent_categoryid)
    {
        global $DB;
        // $categoryrecord = $DB->get_records('course_categories', array('parent' => 0));
        $result = $DB->get_fieldset_sql("SELECT id FROM {course_categories} WHERE path LIKE '/$parent_categoryid' OR path LIKE '/$parent_categoryid/%'");

        return $result;
    }

    public function get_enrolled_user_list($category_id, $course_id)
    {
        global $DB;
        if ($category_id == '') {
            //get all user list
            if ($course_id) {
                $where = " WHERE c.id = $course_id ";
            } else {
                $where = '';
            }
            $sql = "
                 SELECT u.id,u.firstname,u.lastname
                    FROM mdl_course c
                    JOIN mdl_context ct ON c.id = ct.instanceid
                    JOIN mdl_role_assignments ra ON ra.contextid = ct.id
                    JOIN mdl_user u ON u.id = ra.userid
                    JOIN mdl_role r ON r.id = ra.roleid
                    $where
                    GROUP BY u.id
                    ORDER BY u.firstname
                     ";
            $db_result = $DB->get_records_sql($sql);
            return $db_result;
        } else {

            if (!empty($course_id)) {
                $course_id_list = [$course_id];
            } else {
                $category_id_list = self::get_child_category_id($category_id);
                $course_id_list = $DB->get_fieldset_sql("SELECT id FROM {course} WHERE category IN (" . implode(',', $category_id_list) . ") ");
            }


            $sql = "
                 SELECT u.id,u.firstname,u.lastname
                    FROM mdl_course c
                    JOIN mdl_context ct ON c.id = ct.instanceid
                    JOIN mdl_role_assignments ra ON ra.contextid = ct.id
                    JOIN mdl_user u ON u.id = ra.userid
                    JOIN mdl_role r ON r.id = ra.roleid
                    where c.id IN (" . implode(',', $course_id_list) . ")
                    GROUP BY u.id
                    ORDER BY u.firstname
                     ";
            $db_result = $DB->get_records_sql($sql);
            return $db_result;
        }
    }


    public static function getvideo_url_parameters()
    {
        return new external_function_parameters(
            array(
                "studentClass" => new external_value(PARAM_RAW, "studentClass"),
                "resource" => new external_value(PARAM_RAW, "resource"),
                "role" => new external_value(PARAM_RAW, "role")
            )
        );
    }
    //     public static function getvideo_url_returns() {
    //        return new external_multiple_structure(
    //            new external_single_structure(
    //                array(
    //                    'url' => new external_value(PARAM_RAW, 'settings content text')
    //                )
    //            )
    //        );
    //    }

    public static function getvideo_url_returns()
    {
        return new external_single_structure(
            array(
                'url' => new external_value(PARAM_RAW, 'settings content text'),
                'error' => new external_value(PARAM_RAW, 'settings content text')
            )
        );
    }

    public function getvideo_url($studentClass, $resource, $role='STU')
    {
        global  $USER;
       
        $params = self::validate_parameters(self::getvideo_url_parameters(), array('studentClass' => $studentClass, 'resource' => $resource,'role' => $role));
        $payload = array(
            "issued_at" => time() + 20,
        );
        $key = "hg}8F'rhzN:BwXY8";
        $jwtToken = \Firebase\JWT\JWT::encode($payload, $key);


        $payload = array(
            "issued_at" => time() + 20,
        );

        // $url = "https://stgvendorapi.fliplearn.com/user";
        $url = "https://api.fliplearn.com/user";
        $data = [
            [
                "firstName" => $USER->firstname,
                "mobileNumber" => $USER->email,
                "emailId" => $USER->email,
                "role" => $role,
                "class" => $studentClass
            ]
        ];

        $response = self::callCurl($url, 'POST', $data, $payload);
       
        if($response->error){
            return ["url" => '','error'=>$response->error->errorMessage];
        }

        $response2 = '';
        if (!$response->error) {


            // $url="https://stgvendorapi.fliplearn.com/resource/url";
            $url = "https://api.fliplearn.com/resource/url";
            $payload = [
                "uuid" => $response->response->user->fliplearnUserId,
                "resource" => $resource,
                "client-code" => 'edac',
                "issued_at" => time() + 20,
            ];
            $response2 = self::callCurl($url, 'GET', '', $payload);
        }

        return ["url" => $response2->response->url ? $response2->response->url : '','error'=>$response2->error];
    }

    function callCurl($url, $method, $data, $payload)
    {

        $key = "hg}8F'rhzN:BwXY8";
        $jwtToken = \Firebase\JWT\JWT::encode($payload, $key);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Payload: ' . $jwtToken,
                'client-code: edac',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $responseJSON = json_decode($response);
        return $responseJSON;
    }

    public static function getMobileVersion_parameters()
    {
        return new external_function_parameters(array());
    }

    public static function getMobileVersion_returns()
    {
        return new external_single_structure(
            array(
                'version_code' => new external_value(PARAM_RAW, 'settings content text'),
                'version_type' => new external_value(PARAM_RAW, 'settings content text')
            )
        );
    }
    public function getMobileVersion()
    {
        global $DB;
        $sql = "
             SELECT version_code, version_type FROM {mobile_app_version} ORDER BY `mdl_mobile_app_version`.`version_code`  DESC LIMIT 1
                 ";
        return  $db_result = $DB->get_record_sql($sql);
    }
}
