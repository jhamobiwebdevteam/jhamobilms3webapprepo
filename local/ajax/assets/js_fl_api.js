require(['core/first','jquery','jqueryui','core/ajax'],function(core, $, bootstrap, ajax){
    
    $('.btn-play-video').click(function(){
        let tr = $(this).closest('tr');
        let studentClass = $(tr).find("td[name='studentClass']").text();
        let resource = $(tr).find("td[name='videoResource']").text();
        let role = 'STU';
            if(!$('body').hasClass('role-student')){
                role='TEA';
            }

        // let studentClass= $(this).attr("data-class");
        if(studentClass==''){
            studentClass = $(this).attr("data-class");
        }

        if(resource==''){
            resource = $(this).attr("data-resource");
        }

        const playbtn = $(this);
        $(playbtn).find('i.fa-circle-o-notch').show();
         ajax.call([{
                methodname:'log_ajax_getvideourl',
                args:{
                    'studentClass':studentClass,
                    'resource':resource,
                    'role':role
                },
        }])[0].done(function(response){
        console.log(response);
        $(playbtn).find('i.fa-circle-o-notch').hide();
        $('#vieoModal iframe').attr('src',response.url);
        if(response.url==''){
            $('#vieoModal #fl-error-message').html("<h3 style='color:red;background:gainsboro;font-weight:bold;'>"+response.error+"</h3>");
            
         }
        $('#vieoModal').show();
    //    var win = window.open(response.url, '_blank');
    //         win.focus();
        return;
        }).fail(function(err){
            console.log(err);
             $(playbtn).find('i.fa-circle-o-notch').hide();
            return;
        });
    });

    $('#vieoModal .video-close').click(function(){
        $('#vieoModal').hide();
        $('#vieoModal iframe').attr('src','');
        $('#vieoModal #fl-error-message').html("");
    });

    $('#vieoModal .video-refresh').click(function(){
       
        $('#vieoModal iframe').attr('src', $('#vieoModal iframe').attr('src'));
    });
});

