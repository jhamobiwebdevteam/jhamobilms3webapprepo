/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require(['core/first','jquery','jqueryui','core/ajax'],function(core, $, bootstrap, ajax){
    
    location.queryString = {};
    location.search.substr(1).split("&").forEach(function (pair) {
        if (pair === "") return;
        var parts = pair.split("=");
        location.queryString[parts[0]] = parts[1] &&
            decodeURIComponent(parts[1].replace(/\+/g, " "));
    });
    $('#menucategoryid').change(function(){
        
        var selectedCategoryId = $(this).val();
        $("select#menuid").html('<option>loading...</option>');
        ajax.call([{
                methodname:'log_ajax_getcourselist',
                args:{
                    'category_id':selectedCategoryId
                },
        }])[0].done(function(response){
        //console.log(response);
        $("select#menuid").html('');
        $("<option>").val('').html("All Courses").appendTo("select#menuid");
        for(var i=0; i<response.length;i++){
            var selectedCourse = location.queryString.id==response[i].id?' selected ':'';
            $("<option "+selectedCourse+">").val(response[i].id).html(response[i].fullname).appendTo("select#menuid");
        }
       // $("select#menuid").val(location.queryString.id);
        $('#menuid').change();
        return;
        }).fail(function(err){
            console.log(err);
            return;
        });
    });
    
    $('#menuid').change(function(){
        
        var selectedCourseId = $(this).val();
        var selectedCategoryId = $('#menucategoryid').val();
         $("select#menuuser optgroup[label='User']").html('<option>loading...</option>');
        ajax.call([{
                methodname:'log_ajax_getuserlist',
                args:{
                    'category_id':selectedCategoryId,
                    'course_id':selectedCourseId
                },
        }])[0].done(function(response){
//        console.log(response);
        $("select#menuuser optgroup[label='User']").html('');
//        $("<option>").val('').html("All participants").appendTo("select#menuuser");
        for(var i=0; i<response.length;i++){
            
            $("<option>").val(response[i].id).html(response[i].firstname+' '+response[i].lastname).appendTo("select#menuuser optgroup[label='User']");
        }
        return;
        }).fail(function(err){
            console.log(err);
            return;
        });
    });
    $('#menucategoryid').change();
});