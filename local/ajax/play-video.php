<?php


require(__DIR__.'/../../config.php');

global $COURSE, $USER;

$context = get_context_instance(CONTEXT_COURSE,$COURSE->id);



$roles = get_user_roles($context, $USER->id);

$payload = array(
    "issued_at" => time() + 20,
);
$role ='TEA';
$studentClass = $_GET['studentClass'];
$videoResource = $_GET['videoResource'];

// $url = "https://stgvendorapi.fliplearn.com/user";
$url = "https://api.fliplearn.com/user";
$data = [
    [
      "firstName" => $USER->firstname,
      "mobileNumber" => $USER->email,
      "emailId" => $USER->email,
      "role" => $role,
      "class" => $studentClass
  ]
];
$response = callCurl($url,'POST',$data,$payload);
if($response->error){
  echo $response->error->errorMessage;
  die;
}

//start:: curl for get video url
if(!$response->error){
  
  // $url="https://stgvendorapi.fliplearn.com/resource/url";
  $url = "https://api.fliplearn.com/resource/url";
  
    $payload = [
      "uuid"=> $response->response->user->fliplearnUserId,
      "resource"=> $videoResource,
      "client-code"=>'edac',
      "issued_at"=> time() + 20,
    ];
    $response2 = callCurl($url,'GET','',$payload);
  
    if($response2->error){
      echo $response2->error->errorMessage;
      die;
    }

    $videoUrl = $response2->response->url;
    echo "<center> <iframe allowfullscreen='true' webkitallowfullscreen='true' mozallowfullscreen='true' src='$videoUrl' width='100%' height='100%'>
    </iframe></center>";
die;

}


function callCurl($url,$method,$data,$payload){

    $key = "hg}8F'rhzN:BwXY8";
    $jwtToken = \Firebase\JWT\JWT::encode($payload, $key);

    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_POSTFIELDS =>json_encode($data),
      CURLOPT_HTTPHEADER => array(
        'Payload: '.$jwtToken,
        'client-code: edac',
        'Content-Type: application/json'
      ),
    ));

  $response = curl_exec($curl);
  curl_close($curl);
  $responseJSON = json_decode($response);
  return $responseJSON;
}