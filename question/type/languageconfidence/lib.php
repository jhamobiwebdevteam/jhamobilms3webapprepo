<?php
/**
 * Standard Moodle hooks for the Language Confidence question type.
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Checks file access for the Language Confidence question type.
 *
 * @category files
 *
 * @param stdClass $course course object
 * @param stdClass $cm course module object
 * @param stdClass $context context object
 * @param string $filearea file area
 * @param array $args extra arguments
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function qtype_languageconfidence_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = []) {
    global $CFG;
    require_once($CFG->libdir . '/questionlib.php');
    question_pluginfile($course, $context, 'qtype_languageconfidence', $filearea, $args, $forcedownload, $options);
}


function qtype_languageconfidence_ensure_api_config_is_set() {
    global $CFG;

    if (empty($CFG->languageconfidence_apikey)) {
        throw new \Exception('qtype_languageconfidence plugin requires $CFG->languageconfidence_apikey to be set in config.php');
    }
}
