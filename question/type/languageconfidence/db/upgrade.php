<?php
/**
 * Language Confidence question type db upgrade script
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/question/type/languageconfidence/lib.php');

/**
 * Upgrade code for the Language Confidence question type.
 *
 * @param int $oldversion the version we are upgrading from.
 * @return bool
 */
function xmldb_qtype_languageconfidence_upgrade($oldversion) {
    global $DB;

    qtype_languageconfidence_ensure_api_config_is_set();

    $dbman = $DB->get_manager();

    if ($oldversion < 2021060400) {
        $table = new xmldb_table('qtype_langconfid_options');
        $field = new xmldb_field('speechphrase', XMLDB_TYPE_TEXT, null, null, null, null, null, 'mediatype');

        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2021060400, 'qtype', 'languageconfidence');
    }

    return true;
}
