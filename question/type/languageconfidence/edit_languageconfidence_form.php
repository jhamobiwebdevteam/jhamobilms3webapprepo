<?php
/**
 * Defines the editing form for Language Confidence questions.
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/question/type/edit_question_form.php');


/**
 * The editing form for Language Confidence questions.
 *
 * @copyright 2021 Language Confidence
 */
class qtype_languageconfidence_edit_form extends question_edit_form {

    protected function definition_inner($mform) {
        // Field for speechphrase.
        $mform->addElement('text', 'speechphrase', get_string('speechphrase', 'qtype_languageconfidence'),
                array('maxlength' => 1000, 'size' => 100));
        $mform->addHelpButton('speechphrase', 'speechphrase', 'qtype_languageconfidence');
        $mform->addRule('speechphrase', null, 'required', null, 'client');
        $mform->setType('speechphrase', PARAM_TEXT);

        // Field for timelimitinseconds.
        $mform->addElement('duration', 'timelimitinseconds', get_string('timelimit', 'qtype_languageconfidence'),
                ['units' => [60, 1], 'optional' => false]);
        $mform->addHelpButton('timelimitinseconds', 'timelimit', 'qtype_languageconfidence');
        $mform->setDefault('timelimitinseconds', qtype_languageconfidence::DEFAULT_TIMELIMIT);
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Validate placeholders in the question text.
        $placeholdererrors = (new qtype_languageconfidence)->validate_widget_placeholders($data['questiontext']['text'], qtype_languageconfidence::MEDIA_TYPE_AUDIO);
        if ($placeholdererrors) {
            $errors['questiontext'] = $placeholdererrors;
        }

        // Validate the speech phrase.
        if (
            !array_key_exists('speechphrase', $data) ||
            !is_string($data['speechphrase']) ||
            strlen($data['speechphrase']) < 1
        ) {
            $errors['speechphrase'] = get_string('err_speechphraseempty', 'qtype_languageconfidence');
        }

        // Validate the time.
        $maxtimelimit = get_config('qtype_languageconfidence', 'timelimit');
        if ($data['timelimitinseconds'] <= 0) {
            $errors['timelimitinseconds'] = get_string('err_timelimitpositive', 'qtype_languageconfidence');
        } else if ($data['timelimitinseconds'] > $maxtimelimit) {
            $errors['timelimitinseconds'] = get_string('err_timelimit', 'qtype_languageconfidence',
                    format_time($maxtimelimit));
        }

        return $errors;
    }

    public function qtype() {
        return 'languageconfidence';
    }
}
