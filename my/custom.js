require(['jquery'],function($){

    $(document).ready(function(){        
        let button = '<i style="font-size: 1em;float: right;color: #8e8e8e;cursor: pointer;" class="fa fa-caret-up" id="toggleBtn"></i>';
        let button2 = '<i style="font-size: 1em;float: right;color: #8e8e8e;cursor: pointer;" class="fa fa-caret-up" id="toggleBtn2"></i>';
        $(".block_calendar_month .card-body h5").append(button);    
        $(".block_badges .card-body h5").append(button2);     
       $(".block_badges .card-body div").slideToggle();
       $(".block_calendar_month .card-body div:first").slideToggle();
        $(document).on('click','#toggleBtn',function(){
            if($("#toggleBtn").attr("class") === 'fa fa-caret-up'){              
                $("#toggleBtn").removeClass('fa fa-caret-up').addClass('fa fa-caret-down');
            }else{                
                $("#toggleBtn").removeClass('fa fa-caret-down').addClass('fa fa-caret-up');
            } 
            $(".block_calendar_month .card-body div:first").slideToggle();
        });
                
        $(document).on('click','#toggleBtn2',function(){
            if($("#toggleBtn2").attr("class") === 'fa fa-caret-up'){                             
                $("#toggleBtn2").removeClass('fa fa-caret-up').addClass('fa fa-caret-down');
            }else{               
                $("#toggleBtn2").removeClass('fa fa-caret-down').addClass('fa fa-caret-up');
            } 
            $(".block_badges .card-body div").slideToggle();
        });
        
    });
});