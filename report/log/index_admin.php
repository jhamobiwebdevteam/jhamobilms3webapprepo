<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays different views of the logs.
 *
 * @package    report_log
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/report/log/locallib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/lib/tablelib.php');
$CFG->debugusers = '2';
@error_reporting(E_ALL | E_STRICT); // NOT FOR PRODUCTION SERVERS!
@ini_set('display_errors', '1');    // NOT FOR PRODUCTION SERVERS!

$CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
$CFG->debugdisplay = 1;             // NOT FOR PRODUCTION SERVERS!


$id          = optional_param('id', 0, PARAM_INT);// Category ID.
// $group       = optional_param('group', 0, PARAM_INT); // Group to display.
$user        = optional_param('user', 0, PARAM_INT); // User to display.
$date        = optional_param('date', 0, PARAM_INT); // Date to display.
$modid       = optional_param('modid', 0, PARAM_ALPHANUMEXT); // Module id or 'site_errors'.
$modaction   = optional_param('modaction', '', PARAM_ALPHAEXT); // An action as recorded in the logs.
$page        = optional_param('page', '0', PARAM_INT);     // Which page to show.
$perpage     = optional_param('perpage', '100', PARAM_INT); // How many per page.
$showcourses = optional_param('showcourses', false, PARAM_BOOL); // Whether to show courses if we're over our limit.
$showusers   = optional_param('showusers', false, PARAM_BOOL); // Whether to show users if we're over our limit.
$chooselog   = optional_param('chooselog', false, PARAM_BOOL);
$logformat   = optional_param('download', '', PARAM_ALPHA);
$logreader   = optional_param('logreader', '', PARAM_COMPONENT); // Reader which will be used for displaying logs.
$edulevel    = optional_param('edulevel', -1, PARAM_INT); // Educational level.
$origin      = optional_param('origin', '', PARAM_TEXT); // Event origin.

$params = array();
// chooselog=1 & showusers=1 & showcourses=0 & id=65 & user= & date= & modid= & modaction = &edulevel=-1
// chooselog=1 & showusers=0 & showcourses=0 & id=88 & user=12 & date=7 & modid= & modaction= & edulevel=-1 & logreader=logstore_standard
if (!empty($id)) {
    $params['id'] = $id;
} else {
    $site = get_site();
    $id = $site->category;
}
// if ($group !== 0) {
//     $params['group'] = $group;
// }
if ($user !== 0) {
    $params['user'] = $user;
}
if ($date !== 0) {
    $params['date'] = $date;
}
if ($modid !== 0) {
    $params['modid'] = $modid;
}
if ($modaction !== '') {
    $params['modaction'] = $modaction;
}
if ($page !== '0') {
    $params['page'] = $page;
}
if ($perpage !== '100') {
    $params['perpage'] = $perpage;
}
if ($showcourses) {
    $params['showcourses'] = $showcourses;
}
if ($showusers) {
    $params['showusers'] = $showusers;
}
if ($chooselog) {
    $params['chooselog'] = $chooselog;
}
if ($logformat !== '') {
    $params['download'] = $logformat;
}
if ($logreader !== '') {
    $params['logreader'] = $logreader;
}
if (($edulevel != -1)) {
    $params['edulevel'] = $edulevel;
}
if ($origin !== '') {
    $params['origin'] = $origin;
}
// Legacy store hack, as edulevel is not supported.
if ($logreader == 'logstore_legacy') {
    $params['edulevel'] = -1;
    $edulevel = -1;
}
$url = new moodle_url("/report/log/index_admin.php", $params);
$selectedcatid = $id; 
$PAGE->set_url('/report/log/index_admin.php', array('id' => $id));
$PAGE->set_pagelayout('report');
// chooselog=1 & showusers=1 & showcourses=0 & id=65 & user= & date= & modid= & modaction = &edulevel=-1
// chooselog=1 & showusers=0 & showcourses=0 & id=88 & user=12 & date=7 & modid= & modaction= & edulevel=-1 & logreader=logstore_standard
require_login();
$context = context_system::instance();
$PAGE->set_context($context);
 
$showreport = true; 
$order = "timecreated ASC"; 
if (!function_exists('array_key_first')) {
    function array_key_first(array $arr) {
        foreach($arr as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}
//////////////////Filters list//////////////////
//////////////////get category list//////////////////

if (count($DB->get_records('course_categories', array('parent' => 0)))>0) {
    $categoryrecord = $DB->get_records('course_categories', array('parent' => 0));
    foreach ($categoryrecord as $category) {
        $categories[$category->id] = format_string($category->name);
    }
} else {
    $categories = array();
}
if(empty($id)) {
    $categoryid = array_key_first($categories);
} else {
    $categoryid = $id;
}

if (!empty($page)) {
    $strlogs = get_string('logs'). ": ". get_string('page', 'report_log', $page + 1);
} else {
    $strlogs = get_string('logs');
}
// print_r($categories[$categoryid]);die;
if(empty($id)) {
    $PAGE->set_title($categories[$categoryid] .': '. $strlogs);
    $PAGE->set_heading($categories[$categoryid]);
} else {
    $PAGE->set_title($categories[$id] .': '. $strlogs);
    $PAGE->set_heading($categories[$id]);
}
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////get user list//////////////////
$course_context = array();
$course_user = array();
$cat_courses = array();
if (!empty($categoryid)) {
    $getsubcategory = $DB->get_records_sql("SELECT cc. * FROM {course_categories} as cc WHERE cc.path LIKE '/".$categoryid."/%' ORDER BY id ASC");
    // echo '<pre>';print_r($getsubcategory);die;
    foreach ($getsubcategory as $key => $subcategory) {
        $getCourses = $DB->get_records('course', array('category' => $subcategory->id));
        foreach ($getCourses as $key => $getCourse) {
            $cat_courses[$getCourse->id] = $getCourse;
            $context = context_course::instance($getCourse->id);
            $limitfrom = empty($showusers) ? 0 : '';
            $limitnum  = empty($showusers) ? COURSE_MAX_USERS_PER_DROPDOWN + 1 : '';
            $courseusers = get_enrolled_users($context, $withcapability='',$groupid = 0,$userfields = 'u.*',$orderby = '',$limitfrom = 0,$limitnum = 0);
            array_push($course_user, $courseusers);
            $course_context[$context->id]=$context->id;
        }
    }
    if (count($course_user)>0) {
        $showusers = 1;
    }
}
if (count($course_context)>0) {
    $contextids = implode(',',$course_context);
} else {
    $contextids = $context->id;
}
$users = array();
// if ($showusers) {
    if (count($course_user)>0) {
        foreach ($course_user as $key => $courseuser) {
            if(count($courseuser)>0){
                foreach ($courseuser as $key2 => $courseuser2) {
                    // echo "@**********************";
                    // print_r($courseuser[$key2]);
                    if (!array_key_exists($courseuser[$key2]->id, $users)) {
                        $users[$key2] = $courseuser[$key2]->firstname.' '.$courseuser[$key2]->lastname;
                    }
                }
            }
        }
    }
    $users[$CFG->siteguest] = get_string('guestuser');
    $users['t1'] = 'Teacher';
    $users['p2'] = 'Parent';
    $users['s3'] = 'Student';
// }
///////////////////////date list///////////////////////////

    $dates = array();
    $activities = array();

    $strftimedate = get_string("strftimedate");
    $strftimedaydate = get_string("strftimedaydate");
    $timenow = time(); // GMT.
    $timemidnight = usergetmidnight($timenow);
    $dates = array("7"=>"Last 7 Days","30"=>"Last 30 Days","$timemidnight" => get_string("today").", ".userdate($timenow, $strftimedate) );
    // print_r($cat_courses);
    ///////////////////////////////activities list//////////////////////////////////
    $sitecontext = context_system::instance();
    // if ($course->id == SITEID && has_capability('report/log:view', $sitecontext)) {
    //     $activities["site_errors"] = get_string("siteerrors");
    // }

    foreach ($cat_courses as $cat_course) {
        if (!$cat_course->startdate or ($cat_course->startdate > $timenow)) {
            $cat_course->startdate = $cat_course->timecreated;
        }
        $numdates = 1;
        while ($timemidnight > $cat_course->startdate and $numdates < 365) {
                $timemidnight = $timemidnight - 86400;
                $timenow = $timenow - 86400;
                $dates["$timemidnight"] = userdate($timenow, $strftimedaydate);
                $numdates++;
        }
        ///////////////////////////////activities list//////////////////////////////////
        $modinfo = get_fast_modinfo($cat_course);
        if (!empty($modinfo->cms)) {
            $section = 0;
            $thissection = array();
            foreach ($modinfo->cms as $cm) {
                // Exclude activities that aren't visible or have no view link (e.g. label). Account for folders displayed inline.
                if (!$cm->uservisible || (!$cm->has_view() && strcmp($cm->modname, 'folder') !== 0)) {
                    continue;
                }
                if ($cm->sectionnum > 0 and $section <> $cm->sectionnum) {
                    $activities[] = $thissection;
                    $thissection = array();
                }
                $section = $cm->sectionnum;
                $modname = strip_tags($cm->get_formatted_name());
                if (core_text::strlen($modname) > 55) {
                    $modname = core_text::substr($modname, 0, 50)."...";
                }
                if (!$cm->visible) {
                    $modname = "(".$modname.")";
                }
                $key = get_section_name($cat_course, $cm->sectionnum);
                if (!isset($thissection[$key])) {
                    $thissection[$key] = array();
                }
                $thissection[$key][$cm->id] = $modname;
            }
            if (!empty($thissection)) {
                $activities[] = $thissection;
            }
        }
    }
    



/////////////////////////////////////////////////////////////////
$actions = array(
                // 'c' => get_string('create'),
                'r' => get_string('view'),
                'u' => get_string('update'),
                // 'd' => get_string('delete'),
                // 'cud' => get_string('allchanges')
                );

$edulevels = array(
                    -1 => get_string("edulevel"),
                    1 => get_string('edulevelteacher'),
                    2 => 'Viewing',//get_string('edulevelparticipating'),
                    0 => get_string('edulevelother')
                    );

$output = $PAGE->get_renderer('report_log');
echo $output->header();
echo $output->heading(get_string('chooselogs') .':');




echo html_writer::start_tag('form', array('class' => 'logselecform', 'action' => $url, 'method' => 'get'));
echo html_writer::start_div();
echo html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'chooselog', 'value' => '1'));
echo html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'showusers', 'value' => $showusers));
echo html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'showcourses', 'value' => $showcourses));
echo html_writer::label(get_string('selectacategory'), 'menuid', false, array('class' => 'accesshide'));
echo html_writer::select($categories, "id", $selectedcatid, null);
// Add user selector.

ksort($users);
if ($showusers) {
    echo html_writer::label(get_string('selctauser'), 'menuuser', false, array('class' => 'accesshide'));
    echo html_writer::select($users, "user", $user, get_string("allparticipants"));
} else {
    // $users = array();
    if (!empty($user)) {
        $users[$user] = get_string('allparticipants');
        // $users[$reportlog->userid] = $reportlog->get_selected_user_fullname();
    } else {
         $users[0] = get_string('allparticipants');
    }
    echo html_writer::label(get_string('selctauser'), 'menuuser', false, array('class' => 'accesshide'));
    echo html_writer::select($users, "user", $userid, false);
}
    // Add date selector.
    // $dates = $reportlog->get_date_options();
    echo html_writer::label(get_string('date'), 'menudate', false, array('class' => 'accesshide'));
    echo html_writer::select($dates, "date", $date, get_string("alldays"));
    // Add activity selector.
    // $activities = $reportlog->get_activities_list();
    echo html_writer::label(get_string('activities'), 'menumodid', false, array('class' => 'accesshide'));
    echo html_writer::select($activities, "modid", $modid, get_string("allactivities"));
    // Add actions selector.
    echo html_writer::label(get_string('actions'), 'menumodaction', false, array('class' => 'accesshide'));
    echo html_writer::select($actions, 'modaction', $modaction, get_string("allactions"));
    // Add edulevel.
    // $edulevel = $reportlog->get_edulevel_options();
    echo html_writer::label(get_string('edulevel'), 'menuedulevel', false, array('class' => 'accesshide'));
    echo html_writer::select($edulevels, 'edulevel', $edulevel, false);

    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('gettheselogs'),
                'class' => 'btn btn-secondary'));

    echo html_writer::end_div();
        echo html_writer::end_tag('form');
//////////////////////////////////////////////Table view/////////////////////////////////////////////////////////////////////////
if(!empty(optional_param('id', 0, PARAM_INT))) {
    $reportdatas = $DB->get_records_sql("SELECT lsl.id,lsl.target,lsl.component,lsl.contextid,lsl.action, DATE_FORMAT( FROM_UNIXTIME( lsl.timecreated ),'%e %M %Y %h:%i %p') AS tc,concat(u.firstname,' ',u.lastname) AS userfullname, (SELECT CONCAT(u2.firstname,' ', u2.lastname) FROM mdl_user u2 WHERE u2.id = lsl.relateduserid ) AS affected_user,lsl.eventname,lsl.origin,lsl.contextlevel,lsl.courseid,CONCAT('The user with id ',lsl.userid, ' viewed the log report for the course with id ',lsl.courseid) AS `descrip` FROM {logstore_standard_log} lsl LEFT JOIN {user} u ON u.id= lsl.userid WHERE lsl.contextid IN(".$contextids.") limit 100");

    echo '<div class="no-overflow">
            <table class="flexible table table-striped table-hover generaltable generalbox">
                <thead>
                    <tr>
                        <th class="header c0" scope="col">Time<div class="commands"></div></th>
                        <th class="header c1" scope="col">User full name<div class="commands"></div></th>
                        <th class="header c2" scope="col">Affected user<div class="commands"></div></th>
                        <th class="header c3" scope="col">Activity Details<div class="commands"></div></th>
                        <th class="header c4" scope="col">Type of Activity<div class="commands"></div></th>
                        <th class="header c5" scope="col">Action<div class="commands"></div></th>
                        <th class="header c6" scope="col">Description<div class="commands"></div></th>
                        <th class="header c7" scope="col">Origin<div class="commands"></div></th>
                    </tr>
                </thead>
                <tbody>';
            foreach ($reportdatas as $key => $reportdata) {

            echo '  <tr class="" id="uniqueid_r0">
                        <td class="cell c0" id="uniqueid_r0_c0">'.$reportdata->tc.'</td>
                        <td class="cell c1" id="uniqueid_r0_c1">'.$reportdata->userfullname.'</td>
                        <td class="cell c2" id="uniqueid_r0_c2">'.$reportdata->affected_user.'</td>';
                if ($reportdata->contextid) {
                    $context = context::instance_by_id($reportdata->contextid, IGNORE_MISSING);
                    if ($context) {
                            // $contextname = $context->get_context_name(true);
                            echo '<td class="cell c3" id="uniqueid_r0_c3">'.$context->get_context_name(true).'</td>';
                            
                    } else {
                            echo '<td class="cell c3" id="uniqueid_r0_c3">'.get_string("other").'</td>';
                    }
                } else {
                    echo '<td class="cell c3" id="uniqueid_r0_c3">'.get_string("other").'</td>';
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (($reportdata->component === 'core') || ($reportdata->component === 'legacy')) {
                     echo '<td class="cell c4" id="uniqueid_r0_c4">'.get_string("coresystem").'</td>';
                } else if (get_string_manager()->string_exists('pluginname', $reportdata->component)) {
                    echo '<td class="cell c4" id="uniqueid_r0_c4">'.get_string('pluginname', $reportdata->component).'</td>';
                } else {
                    echo '<td class="cell c4" id="uniqueid_r0_c4">'.$reportdata->component.'</td>';
                }   
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // if ($this->filterparams->logreader instanceof logstore_legacy\log\store) {
                //     $eventname = $event->eventname;
                // } else {
                //     $eventname = $event->get_name();
                // }
                // if (($url = $event->get_url()) && empty($this->download)) {
                //     $eventname = $this->action_link($url, $eventname, 'action');
                // }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  echo '
                        <td class="cell c5" id="uniqueid_r0_c5">'.$reportdata->action.'</td>
                        <td class="cell c6" id="uniqueid_r0_c6">'.$reportdata->descrip.'</td>
                        <td class="cell c7" id="uniqueid_r0_c7">'.$reportdata->origin.'</td>
                    </tr>';
            }  
        echo '</tbody>
            </table>
          </div>';
    
}
echo $output->footer();
